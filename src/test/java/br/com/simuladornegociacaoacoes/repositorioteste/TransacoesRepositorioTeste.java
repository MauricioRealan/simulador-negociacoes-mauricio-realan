package br.com.simuladornegociacaoacoes.repositorioteste;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.Test;

import br.com.simuladornegociaoacoes.dominio.Transacoes;
import br.com.simuladornegociaoacoes.repositorio.TransacoesRepositorio;

public class TransacoesRepositorioTeste {

	@Test
	public void listar() throws SQLException {

		TransacoesRepositorio trep = new TransacoesRepositorio();
		ArrayList<Transacoes> lista = trep.listar();

		for (Transacoes t : lista) {
			System.out.println("transacao_id: " + t.getTransacoes_id());
			System.out.println("Empresa: " + t.getEmpresa());
			System.out.println("data: " + t.getData());
			System.out.println("hora: " + t.getHora());
			System.out.println("tipo: " + t.getTipo());
			System.out.println("quantidade: " + t.getQuantidadeacoes());
			System.out.println("valor negociado: " + t.getValornegociado());
			System.out.println();
		}

	}

}
