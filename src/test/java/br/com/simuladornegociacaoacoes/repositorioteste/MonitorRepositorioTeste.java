package br.com.simuladornegociacaoacoes.repositorioteste;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.Ignore;
import org.junit.Test;

import br.com.simuladornegociaoacoes.dominio.Monitor;
import br.com.simuladornegociaoacoes.repositorio.MonitorRepositorio;


public class MonitorRepositorioTeste {

	@Test
	@Ignore
	public void salvar() throws SQLException{
		
		Monitor monitoramento = new Monitor();
		monitoramento.setEmpresa("Disney");
		monitoramento.setPrecovenda(12.00F);
		monitoramento.setPrecocompra(12.50F);
		monitoramento.setValorparaacao(1800.00F);
		
		MonitorRepositorio repositorio = new MonitorRepositorio();
		repositorio.salvar(monitoramento);
	
	}
	
	@Test
	@Ignore
	public void listar() throws SQLException{
		
		MonitorRepositorio rep = new MonitorRepositorio();
		ArrayList<Monitor> lista = rep.listar();
		
		for(Monitor m : lista) {
			System.out.println("Monitor_id: " + m.getMonitor_id());
			System.out.println("Empresa: " + m.getEmpresa());
			System.out.println("Preco Venda: " + m.getPrecovenda());
			System.out.println("Preco Compra: " + m.getPrecocompra());
			System.out.println("Valor para Acoes: " + m.getValorparaacao());
			System.out.println();
		}
		
	}
	
	@Test
	public void editarPrecos() throws SQLException{
		
		MonitorRepositorio rep = new MonitorRepositorio();
		rep.editarPrecos(1L, 13.50F, 9.80F);
		
	}
	
}
