package br.com.simuladornegociacaoacoes.repositorioteste;

import java.sql.SQLException;

import org.junit.Ignore;
import org.junit.Test;

import br.com.simuladornegociaoacoes.dominio.Contas;
import br.com.simuladornegociaoacoes.repositorio.ContasRepositorio;


public class ContasRepositorioTeste {

	@Test
	@Ignore
	public void salvar() throws SQLException{
		
		Contas conta = new Contas();
		conta.setConta_id(12345L);
		conta.setNome("Fulano de Teste");
		conta.setSaldo(680.00F);
		conta.setQtdacoes(0.00F);
		conta.setQtdnegociacoes(0);
		
		ContasRepositorio repositorio = new ContasRepositorio();
		repositorio.salvar(conta);
	}
	
	@Test
	public void buscarConta() throws SQLException{
		
		ContasRepositorio contas = new ContasRepositorio();
		Contas conta = contas.buscarConta(12345L);
		
		System.out.println("Conta: " + conta.getConta_id());
		System.out.println("Nome: " + conta.getNome());
		System.out.println("Saldo: " + conta.getSaldo());
		System.out.println("QTD Acoes: " + conta.getQtdacoes());
		System.out.println("QTD Negociacoes: " + conta.getQtdnegociacoes());
		
	}
	
}
