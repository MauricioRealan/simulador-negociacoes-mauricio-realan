package br.com.simuladornegociacaoacoes.utilteste;

import java.sql.SQLException;

import org.junit.Test;

import br.com.simuladornegociaoacoes.util.EmissorRelatorios;

public class EmissorRelatoriosTeste {

	@Test
	public void gerarRelatorio() {
		
		EmissorRelatorios relatorio = new EmissorRelatorios();
		try {
			relatorio.gerarRelatorio();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
}
