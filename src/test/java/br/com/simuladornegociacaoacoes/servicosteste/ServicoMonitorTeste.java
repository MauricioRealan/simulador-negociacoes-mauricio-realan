package br.com.simuladornegociacaoacoes.servicosteste;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.Ignore;
import org.junit.Test;

import br.com.simuladornegociaoacoes.dominio.Monitor;
import br.com.simuladornegociaoacoes.servicos.ServicoMonitor;

public class ServicoMonitorTeste {

	@Test
	@Ignore
	public void guardar() throws SQLException{
		
		ServicoMonitor monitor = new ServicoMonitor();
		monitor.guardarMonitoramento("ESComp", "10.00", "10.50", "2750.00", "178");
		
	}
	
	@Test
	@Ignore
	public void listar() throws SQLException{
		
		ServicoMonitor monitor = new ServicoMonitor();
		ArrayList<Monitor> listaMonitoramento = monitor.listarMonitoramento();
		
		for(Monitor m : listaMonitoramento) {
			System.out.println("Monitor_id: " + m.getMonitor_id());
			System.out.println("Empresa: " + m.getEmpresa());
			System.out.println("Preco Venda: " + m.getPrecovenda());
			System.out.println("Preco Compra: " + m.getPrecocompra());
			System.out.println("Valor para Acoes: " + m.getValorparaacao());
			System.out.println();
		}
		
	}
	
	@Test
	@Ignore
	public void buscar() throws SQLException{
		
		ServicoMonitor monitor = new ServicoMonitor();
		Monitor monitoramento = monitor.consultarMonitoramento("178");
		
			System.out.println("Monitor_id: " + monitoramento.getMonitor_id());
			System.out.println("Empresa: " + monitoramento.getEmpresa());
			System.out.println("Preco Venda: " + monitoramento.getPrecovenda());
			System.out.println("Preco Compra: " + monitoramento.getPrecocompra());
			System.out.println("Valor para Acoes: " + monitoramento.getValorparaacao());
			System.out.println();
		
	}
	
	@Test
	public void excluir() throws SQLException{
		
		ServicoMonitor monitor = new ServicoMonitor();
		monitor.excluirMonitoramento("12345");
		
	}
	
}
