package br.com.simuladornegociacaoacoes.servicosteste;

import java.sql.SQLException;

import org.junit.Ignore;
import org.junit.Test;

import br.com.simuladornegociaoacoes.dominio.Contas;
import br.com.simuladornegociaoacoes.servicos.ServicoContas;

public class ServicoContasTeste {

	@Test
	@Ignore
	public void guardar() throws SQLException{
		
		ServicoContas contas = new ServicoContas();
		contas.guardarContas("178", "IMDH", "2750.00");
		
	}
	
	@Test
	@Ignore
	public void buscar() throws SQLException{
		
		ServicoContas contas = new ServicoContas();
		Contas conta = contas.buscarConta("178");
		
		System.out.println("Conta: " + conta.getConta_id());
		System.out.println("Nome: " + conta.getNome());
		System.out.println("Saldo: " + conta.getSaldo());
		System.out.println("QTD Acoes: " + conta.getQtdacoes());
		System.out.println("QTD Negociacoes: " + conta.getQtdnegociacoes());
		
	}
	
	@Test
	public void excluir() throws SQLException{
		
		ServicoContas contas = new ServicoContas();
		contas.excluirConta("987654");
		
	}
	
}
