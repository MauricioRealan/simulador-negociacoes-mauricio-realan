package br.com.simuladornegociaoacoes.util;

import java.util.concurrent.ThreadLocalRandom;

public class EmissorPrecos {

	public String simularEmissaoPrecos(Float PC, Float PV) {

		Double novoPC = 10.00, novoPV = 10.00;
		
		Float Pmin = PC - (PC / 10.00F);
		Float Pmax = PV + (PV / 10.00F);

		if (Pmin < Pmax) {
			novoPC = ThreadLocalRandom.current().nextDouble(Pmin, Pmax);
			novoPV = ThreadLocalRandom.current().nextDouble(Pmin, Pmax);
		}

		if (Pmax < Pmin) {
			novoPC = ThreadLocalRandom.current().nextDouble(Pmax, Pmin);
			novoPV = ThreadLocalRandom.current().nextDouble(Pmax, Pmin);
		}

		String temp = String.valueOf(novoPC);
		String temp2 = String.valueOf(novoPV);

		return (temp + "/" + temp2);

	}

}
