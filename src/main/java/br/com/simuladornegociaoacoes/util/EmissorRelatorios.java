package br.com.simuladornegociaoacoes.util;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.simuladornegociaoacoes.dominio.Transacoes;
import br.com.simuladornegociaoacoes.repositorio.TransacoesRepositorio;

public class EmissorRelatorios {

	public void gerarRelatorio() throws SQLException{
		
		TransacoesRepositorio transacoes = new TransacoesRepositorio();
		ArrayList<Transacoes> lista = transacoes.listar();
		
		System.out.println("--------------- Relatório de Transações ---------------");
		for (Transacoes t : lista) {
			System.out.println("---------------");
			System.out.println("Empresa: " + t.getEmpresa());
			System.out.println("Tipo Transacao: " + t.getTipo());
			System.out.println("Data: " + t.getData() + " Hora:" + t.getHora());
			System.out.println("Quantidade Negociada: " + t.getQuantidadeacoes());
			System.out.println("Valor negociado: " + t.getValornegociado());
			System.out.println();
		}
		
		try {
			gravarArquivo(lista);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void gravarArquivo(ArrayList<Transacoes> lista) throws IOException{
		
		FileWriter writer = new FileWriter("RelatorioTransacoes.txt", false);
		
		for(Transacoes transacao : lista) {
			writer.write(transacao.getEmpresa());
			writer.write(" : ");
			writer.write(transacao.getTipo());
			writer.write(" : ");
			writer.write(String.valueOf(transacao.getData()));
			writer.write(" : ");
			writer.write(transacao.getHora());
			writer.write(" : ");
			writer.write(String.valueOf(transacao.getQuantidadeacoes()));
			writer.write(" : ");
			writer.write(String.valueOf(transacao.getValornegociado()));
			writer.write("\r\n");
		}
		
		writer.flush();
		writer.close();
		
	}
	
}
