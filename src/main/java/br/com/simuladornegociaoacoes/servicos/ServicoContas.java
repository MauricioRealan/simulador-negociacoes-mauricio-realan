package br.com.simuladornegociaoacoes.servicos;

import java.sql.SQLException;

import org.springframework.stereotype.Service;

import br.com.simuladornegociaoacoes.dominio.Contas;
import br.com.simuladornegociaoacoes.repositorio.ContasRepositorio;

@Service
public class ServicoContas {

	public void guardarContas(String conta, String nome, String saldo) throws SQLException{
		
		Contas novaconta = new Contas();
		novaconta.setConta_id(Long.parseLong(conta));
		novaconta.setNome(nome);
		novaconta.setSaldo(Float.parseFloat(saldo));
		novaconta.setQtdacoes(0.00F);
		novaconta.setQtdnegociacoes(0);
		
		ContasRepositorio contarep = new ContasRepositorio();
		contarep.salvar(novaconta);
	
	}
	
	public Contas buscarConta(String conta) throws SQLException{
		
		Long conta_id = Long.parseLong(conta);
		ContasRepositorio contarep = new ContasRepositorio();
		Contas contabusca = new Contas();
		contabusca = contarep.buscarConta(conta_id);
		
		return contabusca;
	}
	
	public void excluirConta(String conta) throws SQLException{
		
		Long conta_id = Long.parseLong(conta);
		ContasRepositorio contarep = new ContasRepositorio();
		contarep.excluirContas(conta_id);
		
	}
	
}
