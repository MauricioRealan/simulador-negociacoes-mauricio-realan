package br.com.simuladornegociaoacoes.servicos;

import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.stereotype.Service;

import br.com.simuladornegociaoacoes.dominio.Monitor;
import br.com.simuladornegociaoacoes.repositorio.MonitorRepositorio;

@Service
public class ServicoMonitor {

	public void guardarMonitoramento(String empresa, String precovenda, String precocompra, String valorparaacao, String conta_id) throws SQLException{
		
		Monitor novomonitor = new Monitor();
		novomonitor.setEmpresa(empresa);
		novomonitor.setPrecovenda(Float.parseFloat(precovenda));
		novomonitor.setPrecocompra(Float.parseFloat(precocompra));
		novomonitor.setValorparaacao(Float.parseFloat(valorparaacao));
		novomonitor.setConta_id(Long.parseLong(conta_id));
		
		MonitorRepositorio monitorrep = new MonitorRepositorio();
		monitorrep.salvar(novomonitor);
	
	}
	
	public ArrayList<Monitor> listarMonitoramento() throws SQLException{
		
		MonitorRepositorio rep = new MonitorRepositorio();
		ArrayList<Monitor> listaMonitoramento = rep.listar();
		
		return listaMonitoramento;
		
	}
	
	public Monitor buscarMonitoramento(ArrayList<Monitor> monitor, Long conta_id) {
		
		Monitor monitorVinculado = new Monitor();

		for(Monitor m : monitor) {
			if(conta_id.equals(m.getConta_id())) {
				monitorVinculado.setMonitor_id(m.getMonitor_id());
				monitorVinculado.setEmpresa(m.getEmpresa());
				monitorVinculado.setPrecovenda(m.getPrecovenda());
				monitorVinculado.setPrecocompra(m.getPrecocompra());
				monitorVinculado.setValorparaacao(m.getValorparaacao());
				monitorVinculado.setConta_id(m.getConta_id());
			}
		}
		
		return monitorVinculado;
	}
	
	public Monitor consultarMonitoramento(String conta) throws SQLException{
		
		MonitorRepositorio monitor = new MonitorRepositorio();
		Monitor monitorbuscado = monitor.buscarMonitoramento(Long.parseLong(conta));
		return monitorbuscado;
		
	}
	
	public void excluirMonitoramento(String conta) throws SQLException{
		
		MonitorRepositorio monitor = new MonitorRepositorio();
		monitor.excluir(Long.parseLong(conta));
		
	}
	
}
