package br.com.simuladornegociaoacoes.servicos;

import java.sql.SQLException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;

import org.springframework.stereotype.Service;

import br.com.simuladornegociaoacoes.dominio.Contas;
import br.com.simuladornegociaoacoes.dominio.Monitor;
import br.com.simuladornegociaoacoes.dominio.Transacoes;
import br.com.simuladornegociaoacoes.repositorio.ContasRepositorio;
import br.com.simuladornegociaoacoes.repositorio.MonitorRepositorio;
import br.com.simuladornegociaoacoes.repositorio.TransacoesRepositorio;
import br.com.simuladornegociaoacoes.util.EmissorPrecos;

@Service
public class ServicoTransacoes {

	public ArrayList<Monitor> buscarNovosPrecos(ArrayList<Monitor> monitoramento) throws SQLException {

		for (Monitor m : monitoramento) {
			EmissorPrecos emissor = new EmissorPrecos();
			String novopreco = emissor.simularEmissaoPrecos(m.getPrecovenda(), m.getPrecocompra());
			String conv[] = novopreco.split("/");
			Float PC = Float.parseFloat(conv[0]);
			Float PV = Float.parseFloat(conv[1]);
			MonitorRepositorio mrepositorio = new MonitorRepositorio();
			mrepositorio.editarPrecos(m.getMonitor_id(), PV, PC);
		}

		MonitorRepositorio monitor = new MonitorRepositorio();
		ArrayList<Monitor> novosmonitoramentos = monitor.listar();
		return novosmonitoramentos;
	}

	public void avaliarTransacoes(ArrayList<Monitor> monitoramento, Long conta_id) throws SQLException {

		ContasRepositorio contas = new ContasRepositorio();
		Contas conta = contas.buscarConta(conta_id);
		ServicoMonitor servmonitor = new ServicoMonitor();
		Monitor monitorVinculado = servmonitor.buscarMonitoramento(monitoramento, conta_id);

		for (Monitor m : monitoramento) {
			if (conta.getSaldo() > 0) {
				if ((monitorVinculado.getPrecocompra() > m.getPrecovenda()) && (conta.getSaldo() >= m.getPrecovenda())) {
					Monitor monitorVende = m;
					Float novosaldo = comprarAcoes(conta, monitorVinculado, monitorVende);
					conta.setSaldo(novosaldo);
				}
				if ((monitorVinculado.getPrecovenda() < m.getPrecocompra()) && (m.getValorparaacao() >= monitorVinculado.getPrecovenda())) {
					Monitor monitorCompra = m;
					Float novosaldo = venderAcoes(conta, monitorVinculado, monitorCompra);
					conta.setSaldo(novosaldo);
				}
			}
			if (conta.getSaldo() == 0) {
				if ((monitorVinculado.getPrecovenda() < m.getPrecocompra()) && (m.getValorparaacao() >= monitorVinculado.getPrecovenda())) {
					Monitor monitorCompra = m;
					Float novosaldo = venderAcoes(conta, monitorVinculado, monitorCompra);
					conta.setSaldo(novosaldo);
				}
			}

		}

	}

	public Float comprarAcoes(Contas conta, Monitor monitorCompra, Monitor monitorVende) throws SQLException {
		String tipo = "Compra";
		Transacoes novatransacao = new Transacoes();
		Float qtdacoes = conta.getSaldo() / monitorVende.getPrecovenda();
		Float valornegociado = qtdacoes * monitorVende.getPrecovenda();
		conta.setQtdacoes(qtdacoes);
		novatransacao.setEmpresa(monitorVende.getEmpresa());
		novatransacao.setTipo(tipo);
		novatransacao.setData(new Date());
		LocalTime horarioDeEntrada = LocalTime.now();
		String hora = horarioDeEntrada.toString();
		novatransacao.setHora(hora);
		novatransacao.setQuantidadeacoes(qtdacoes);
		novatransacao.setValornegociado(valornegociado);
		TransacoesRepositorio transrepositorio = new TransacoesRepositorio();
		transrepositorio.salvar(novatransacao);
		conta.setQtdacoes(qtdacoes + conta.getQtdacoes());
		conta.setQtdnegociacoes(1 + conta.getQtdnegociacoes());
		Float novosaldo = conta.getSaldo() - valornegociado;
		conta.setSaldo(novosaldo);
		ContasRepositorio editconta = new ContasRepositorio();
		editconta.editarContas(conta);
		monitorVende.setValorparaacao(valornegociado + monitorVende.getValorparaacao());
		monitorCompra.setValorparaacao(monitorCompra.getValorparaacao() - valornegociado);
		MonitorRepositorio monitor = new MonitorRepositorio();
		monitor.editarValor(monitorVende);
		monitor.editarValor(monitorCompra);
		System.out.println("Empresa: " + monitorVende.getEmpresa() + "Valor Compra: " + monitorVende.getPrecovenda() + "Valor Negociado: " + valornegociado);
		return novosaldo;
	}

	public Float venderAcoes(Contas conta, Monitor monitorVende, Monitor monitorCompra) throws SQLException {
		String tipo = "Venda";
		Transacoes novatransacao = new Transacoes();
		Float qtdacoes = monitorCompra.getValorparaacao() / monitorVende.getPrecovenda();
		Float valornegociado = qtdacoes * monitorVende.getPrecovenda();
		conta.setQtdacoes(qtdacoes);
		novatransacao.setEmpresa(monitorCompra.getEmpresa());
		novatransacao.setTipo(tipo);
		novatransacao.setData(new Date());
		LocalTime horarioDeEntrada = LocalTime.now();
		String hora = horarioDeEntrada.toString();
		novatransacao.setHora(hora);
		novatransacao.setQuantidadeacoes(qtdacoes);
		novatransacao.setValornegociado(valornegociado);
		TransacoesRepositorio transrepositorio = new TransacoesRepositorio();
		transrepositorio.salvar(novatransacao);
		Float novosaldo = conta.getSaldo() + valornegociado;
		conta.setQtdacoes(conta.getQtdacoes() - qtdacoes);
		conta.setQtdnegociacoes(1 + conta.getQtdnegociacoes());
		conta.setSaldo(novosaldo);
		ContasRepositorio editconta = new ContasRepositorio();
		editconta.editarContas(conta);
		monitorVende.setValorparaacao(valornegociado + monitorVende.getValorparaacao());
		monitorCompra.setValorparaacao(monitorCompra.getValorparaacao() - valornegociado);
		MonitorRepositorio monitor = new MonitorRepositorio();
		monitor.editarValor(monitorVende);
		monitor.editarValor(monitorCompra);
		System.out.println("Empresa: " + monitorCompra.getEmpresa() + "Valor Venda: " + monitorCompra.getPrecovenda() + "Valor Negociado: " + valornegociado);
		return novosaldo;
	}

}
