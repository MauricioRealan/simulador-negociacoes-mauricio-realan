package br.com.simuladornegociaoacoes.repositorio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.simuladornegociaoacoes.conexao.Conexao;
import br.com.simuladornegociaoacoes.dominio.Contas;

public class ContasRepositorio {

	public void salvar(Contas conta) throws SQLException {

		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO Contas ");
		sql.append("(conta_id, nome, saldo, qtdacoes, qtdnegociacoes) ");
		sql.append("VALUES (?, ?, ?, ?, ?) ");

		Connection conexao = Conexao.conectar();

		PreparedStatement comando = conexao.prepareStatement(sql.toString());
		comando.setLong(1, conta.getConta_id());
		comando.setString(2, conta.getNome());
		comando.setFloat(3, conta.getSaldo());
		comando.setFloat(4, conta.getQtdacoes());
		comando.setInt(5, conta.getQtdnegociacoes());
		comando.executeUpdate();

	}

	public Contas buscarConta(Long conta_id) throws SQLException {

		StringBuilder sql = new StringBuilder();
		sql.append("SELECT conta_id, nome, saldo, qtdacoes, qtdnegociacoes ");
		sql.append("FROM Contas ");
		sql.append("WHERE conta_id = ? ");

		Connection conexao = Conexao.conectar();

		PreparedStatement comando = conexao.prepareStatement(sql.toString());
		comando.setLong(1, conta_id);

		ResultSet resultado = comando.executeQuery();

		Contas conta = null;

		if (resultado.next()) {
			conta = new Contas();
			conta.setConta_id(resultado.getLong("conta_id"));
			conta.setNome(resultado.getString("nome"));
			conta.setSaldo(resultado.getFloat("saldo"));
			conta.setQtdacoes(resultado.getFloat("qtdacoes"));
			conta.setQtdnegociacoes(resultado.getInt("qtdnegociacoes"));
		}

		conexao.close();
		
		return conta;

	}

	public void editarContas(Contas conta) throws SQLException {

		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE Contas ");
		sql.append("SET saldo = ?, qtdacoes = ?, qtdnegociacoes = ? ");
		sql.append("WHERE conta_id = ? ");

		Connection conexao = Conexao.conectar();

		PreparedStatement comando = conexao.prepareStatement(sql.toString());
		comando.setFloat(1, conta.getSaldo());
		comando.setFloat(2, conta.getQtdacoes());
		comando.setInt(3, conta.getQtdnegociacoes());
		comando.setLong(4, conta.getConta_id());

		comando.executeUpdate();
		
		conexao.close();

	}
	
	public void excluirContas(Long conta_id) throws SQLException{
		
		StringBuilder sql = new StringBuilder();
		sql.append("DELETE FROM Contas ");
		sql.append("WHERE conta_id = ? ");
		
		Connection conexao = Conexao.conectar();
		
		PreparedStatement comando = conexao.prepareStatement(sql.toString());
		comando.setLong(1, conta_id);
		
		comando.executeUpdate();
		
	}

}
