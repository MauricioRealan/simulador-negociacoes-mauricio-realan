package br.com.simuladornegociaoacoes.repositorio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.simuladornegociaoacoes.conexao.Conexao;
import br.com.simuladornegociaoacoes.dominio.Transacoes;

public class TransacoesRepositorio {

	public void salvar(Transacoes transacao) throws SQLException {

		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO Transacoes ");
		sql.append("(empresa, data, hora, tipo, quantidade, valornegociado) ");
		sql.append("VALUES (?, ?, ?, ?, ?, ?) ");

		java.util.Date data = new java.util.Date();
		java.sql.Date dataSql = new java.sql.Date(data.getTime());

		Connection conexao = Conexao.conectar();

		PreparedStatement comando = conexao.prepareStatement(sql.toString());
		comando.setString(1, transacao.getEmpresa());
		comando.setDate(2, dataSql);
		comando.setString(3, transacao.getHora());
		comando.setString(4, transacao.getTipo());
		comando.setFloat(5, transacao.getQuantidadeacoes());
		comando.setFloat(6, transacao.getValornegociado());
		comando.executeUpdate();
		
		conexao.close();


	}

	public ArrayList<Transacoes> listar() throws SQLException {

		StringBuilder sql = new StringBuilder();
		sql.append("SELECT transacoes_id, empresa, data, hora, tipo, quantidade, valornegociado ");
		sql.append("FROM Transacoes ");
		sql.append("ORDER BY empresa ASC ");

		Connection conexao = Conexao.conectar();

		PreparedStatement comando = conexao.prepareStatement(sql.toString());

		ResultSet resultado = comando.executeQuery();

		ArrayList<Transacoes> lista = new ArrayList<>();

		while (resultado.next()) {
			Transacoes transacao = new Transacoes();
			transacao.setTransacoes_id(resultado.getLong("transacoes_id"));
			transacao.setEmpresa(resultado.getString("empresa"));
			transacao.setData(resultado.getDate("data"));
			transacao.setHora(resultado.getString("hora"));
			transacao.setTipo(resultado.getString("tipo"));
			transacao.setQuantidadeacoes(resultado.getFloat("quantidade"));
			transacao.setValornegociado(resultado.getFloat("valornegociado"));
			lista.add(transacao);
		}

		conexao.close();
		
		return lista;

	}

}
