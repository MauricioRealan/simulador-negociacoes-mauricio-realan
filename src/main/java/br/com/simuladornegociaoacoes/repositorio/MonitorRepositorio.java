package br.com.simuladornegociaoacoes.repositorio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.simuladornegociaoacoes.conexao.Conexao;
import br.com.simuladornegociaoacoes.dominio.Monitor;

public class MonitorRepositorio {

	public void salvar(Monitor monitoramento) throws SQLException {

		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO Monitor ");
		sql.append("(empresa, precovenda, precocompra, valorparaacao, conta_id) ");
		sql.append("VALUES (?, ?, ?, ?, ?) ");

		Connection conexao = Conexao.conectar();

		PreparedStatement comando = conexao.prepareStatement(sql.toString());
		comando.setString(1, monitoramento.getEmpresa());
		comando.setFloat(2, monitoramento.getPrecovenda());
		comando.setFloat(3, monitoramento.getPrecocompra());
		comando.setFloat(4, monitoramento.getValorparaacao());
		comando.setLong(5, monitoramento.getConta_id());
		comando.executeUpdate();

	}

	public ArrayList<Monitor> listar() throws SQLException {

		StringBuilder sql = new StringBuilder();
		sql.append("SELECT monitor_id, empresa, precovenda, precocompra, valorparaacao, conta_id ");
		sql.append("FROM Monitor ");
		sql.append("ORDER BY empresa ASC ");

		Connection conexao = Conexao.conectar();

		PreparedStatement comando = conexao.prepareStatement(sql.toString());

		ResultSet resultado = comando.executeQuery();

		ArrayList<Monitor> lista = new ArrayList<>();

		while (resultado.next()) {
			Monitor m = new Monitor();
			m.setMonitor_id(resultado.getLong("monitor_id"));
			m.setEmpresa(resultado.getString("empresa"));
			m.setPrecovenda(resultado.getFloat("precovenda"));
			m.setPrecocompra(resultado.getFloat("precocompra"));
			m.setValorparaacao(resultado.getFloat("valorparaacao"));
			m.setConta_id(resultado.getLong("conta_id"));
			lista.add(m);
		}

		conexao.close();
		
		return lista;

	}

	public void editarPrecos(Long monitor_id, Float PV, Float PC) throws SQLException {

		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE Monitor ");
		sql.append("SET precovenda = ?, precocompra = ? ");
		sql.append("WHERE monitor_id = ? ");

		Connection conexao = Conexao.conectar();

		PreparedStatement comando = conexao.prepareStatement(sql.toString());
		comando.setFloat(1, PV);
		comando.setFloat(2, PC);
		comando.setLong(3, monitor_id);

		comando.executeUpdate();
		
		conexao.close();

	}

	public void editarValor(Monitor monitoramento) throws SQLException {

		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE Monitor ");
		sql.append("SET valorparaacao = ? ");
		sql.append("WHERE monitor_id = ? ");

		Connection conexao = Conexao.conectar();

		PreparedStatement comando = conexao.prepareStatement(sql.toString());
		comando.setFloat(1, monitoramento.getValorparaacao());
		comando.setLong(2, monitoramento.getMonitor_id());

		comando.executeUpdate();
		
		conexao.close();

	}

	public Monitor buscarMonitoramento(Long conta) throws SQLException {

		StringBuilder sql = new StringBuilder();
		sql.append("SELECT monitor_id, empresa, precovenda, precocompra, valorparaacao ");
		sql.append("FROM Monitor ");
		sql.append("WHERE conta_id = ? ");

		Connection conexao = Conexao.conectar();

		PreparedStatement comando = conexao.prepareStatement(sql.toString());
		comando.setLong(1, conta);

		ResultSet resultado = comando.executeQuery();

		Monitor monitoramento = null;

		if (resultado.next()) {
			monitoramento = new Monitor();
			monitoramento.setMonitor_id(resultado.getLong("monitor_id"));
			monitoramento.setEmpresa(resultado.getString("empresa"));
			monitoramento.setPrecovenda(resultado.getFloat("precovenda"));
			monitoramento.setPrecocompra(resultado.getFloat("precocompra"));
			monitoramento.setValorparaacao(resultado.getFloat("valorparaacao"));
		}

		conexao.close();
		
		return monitoramento;

	}

	public void excluir(Long conta) throws SQLException {

		StringBuilder sql = new StringBuilder();
		sql.append("DELETE FROM Monitor ");
		sql.append("WHERE conta_id = ? ");

		Connection conexao = Conexao.conectar();

		PreparedStatement comando = conexao.prepareStatement(sql.toString());
		comando.setLong(1, conta);

		comando.executeUpdate();

	}

}
