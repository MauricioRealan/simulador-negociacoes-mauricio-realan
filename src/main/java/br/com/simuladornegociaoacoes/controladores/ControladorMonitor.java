package br.com.simuladornegociaoacoes.controladores;

import java.sql.SQLException;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.simuladornegociaoacoes.dominio.Monitor;
import br.com.simuladornegociaoacoes.servicos.ServicoMonitor;

@RestController
@RequestMapping("/monitoramento")
public class ControladorMonitor {
	
	@RequestMapping("/criar")
	public String criarMonitoramento(@RequestParam("empresa") String empresa, @RequestParam("pvenda") String pvenda, 
			@RequestParam("pcompra") String pcompra, @RequestParam("valoracao") String valoracao, 
			@RequestParam("contaId") String contaId) throws SQLException {
		ServicoMonitor servmonitor = new ServicoMonitor();
		servmonitor.guardarMonitoramento(empresa, pvenda, pcompra, valoracao, contaId);
		return "-- Monitoramento Criado com Sucesso! --";
	}
	
	@RequestMapping("/buscar")
	public String buscarMonitoramento(@RequestParam("conta") String conta) throws SQLException {
		ServicoMonitor servmonitor = new ServicoMonitor();
		Monitor monitorbuscado = servmonitor.consultarMonitoramento(conta);
		return "Empresa: " + monitorbuscado.getEmpresa() + " | " + "Preço Venda: " + monitorbuscado.getPrecovenda() + " | " 
		+ "Preço Compra: " + monitorbuscado.getPrecocompra() + " | " + "Valor para Ações: " + monitorbuscado.getValorparaacao() + " | " 
		+ "ID Monitor: " + monitorbuscado.getMonitor_id();
	
	}
	
	@RequestMapping("/excluir")
	public String excluirMonitoramento(@RequestParam("conta") String conta) throws SQLException {
		ServicoMonitor servmonitor = new ServicoMonitor();
		servmonitor.excluirMonitoramento(conta);
		return "-- Monitoramento Excluido com Sucesso! --";
	
	}
	
}
