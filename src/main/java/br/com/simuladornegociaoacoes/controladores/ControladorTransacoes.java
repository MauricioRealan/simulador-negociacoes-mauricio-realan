package br.com.simuladornegociaoacoes.controladores;

import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.simuladornegociaoacoes.dominio.Monitor;
import br.com.simuladornegociaoacoes.servicos.ServicoMonitor;
import br.com.simuladornegociaoacoes.servicos.ServicoTransacoes;
import br.com.simuladornegociaoacoes.util.EmissorRelatorios;

@RestController
@RequestMapping("/transacoes")
public class ControladorTransacoes {

	@RequestMapping("/iniciar")
	public String iniciarTransacoes(@RequestParam("contaID") String contaID) throws SQLException {

		for (int i = 0; i < 40; i++) {

			ServicoMonitor servmonitor = new ServicoMonitor();
			ArrayList<Monitor> monitoramentos = servmonitor.listarMonitoramento();
			ServicoTransacoes servtransacoes = new ServicoTransacoes();
			ArrayList<Monitor> monitoralterado = servtransacoes.buscarNovosPrecos(monitoramentos);
			Long conta_id = Long.parseLong(contaID);
			servtransacoes.avaliarTransacoes(monitoralterado, conta_id);

			try {
				Thread.sleep(5000);
				System.out.println("-- Transações em Andamento, Aguarde! --");
			} catch (InterruptedException ex) {
				ex.printStackTrace();
				System.out.println("-- Ocorreu um Erro Durante a Execucao das Transacoes! --");
			}

		}
		
		EmissorRelatorios relatorio = new EmissorRelatorios();
		relatorio.gerarRelatorio();
		
		return "-- Transações Concluidas, Relatório Gerado com Sucesso! --";

	}

}
