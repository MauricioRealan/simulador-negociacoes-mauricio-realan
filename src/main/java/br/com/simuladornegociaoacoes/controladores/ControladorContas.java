package br.com.simuladornegociaoacoes.controladores;

import java.sql.SQLException;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.simuladornegociaoacoes.dominio.Contas;
import br.com.simuladornegociaoacoes.servicos.ServicoContas;

@RestController
@RequestMapping("/contas")
public class ControladorContas {

	@RequestMapping("/criar")
	public String criarConta(@RequestParam("conta") String conta, @RequestParam("nome") String nome, @RequestParam("saldo") String saldo) throws SQLException {
		ServicoContas servcontas = new ServicoContas();
		servcontas.guardarContas(conta, nome, saldo);
		return "-- Conta Criada com Sucesso! --";
	}
	
	@RequestMapping("/buscar")
	public String buscarConta(@RequestParam("conta") String conta) throws SQLException {
		ServicoContas servcontas = new ServicoContas();
		Contas contabusca = new Contas();
		contabusca = servcontas.buscarConta(conta);
		return "Conta: " + contabusca.getConta_id() + " | " + "Nome: " + contabusca.getNome() + " | " 
				+ "Saldo: " + contabusca.getSaldo() + " | " + "QTD Ações: " + contabusca.getQtdacoes() + " | " 
				+ "QTD Negociações: " + contabusca.getQtdnegociacoes();
	}
	
	@RequestMapping("/excluir")
	public String excluirConta(@RequestParam("conta") String conta) throws SQLException {
		ServicoContas servcontas = new ServicoContas();
		servcontas.excluirConta(conta);
		return "-- Conta Excluida com Sucesso! --";
	}
	
}
