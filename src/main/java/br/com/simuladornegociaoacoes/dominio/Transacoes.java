package br.com.simuladornegociaoacoes.dominio;

import java.util.Date;

public class Transacoes {

	private Long transacoes_id;
	private String empresa;
	private Date data;
	private String hora;
	private String tipo;
	private Float valornegociado;
	private Float quantidadeacoes;
	
	public Float getQuantidadeacoes() {
		return quantidadeacoes;
	}
	public void setQuantidadeacoes(Float quantidadeacoes) {
		this.quantidadeacoes = quantidadeacoes;
	}
	public Long getTransacoes_id() {
		return transacoes_id;
	}
	public void setTransacoes_id(Long transacoes_id) {
		this.transacoes_id = transacoes_id;
	}
	public String getEmpresa() {
		return empresa;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public Float getValornegociado() {
		return valornegociado;
	}
	public void setValornegociado(Float valornegociado) {
		this.valornegociado = valornegociado;
	}
	
	
	
}
