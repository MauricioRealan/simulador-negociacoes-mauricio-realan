package br.com.simuladornegociaoacoes.dominio;

public class Monitor {

	private Long monitor_id;
	private String empresa;
	private Float precocompra;
	private Float precovenda;
	private Float valorparaacao;
	private Long conta_id;
	
	public Long getConta_id() {
		return conta_id;
	}
	public void setConta_id(Long conta_id) {
		this.conta_id = conta_id;
	}
	public Long getMonitor_id() {
		return monitor_id;
	}
	public void setMonitor_id(Long monitor_id) {
		this.monitor_id = monitor_id;
	}
	public String getEmpresa() {
		return empresa;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public Float getPrecocompra() {
		return precocompra;
	}
	public void setPrecocompra(Float precocompra) {
		this.precocompra = precocompra;
	}
	public Float getPrecovenda() {
		return precovenda;
	}
	public void setPrecovenda(Float precovenda) {
		this.precovenda = precovenda;
	}
	public Float getValorparaacao() {
		return valorparaacao;
	}
	public void setValorparaacao(Float valorparaacao) {
		this.valorparaacao = valorparaacao;
	}
	
	
	
}
