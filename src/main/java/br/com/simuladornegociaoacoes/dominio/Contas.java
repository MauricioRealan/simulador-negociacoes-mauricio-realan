package br.com.simuladornegociaoacoes.dominio;

public class Contas {

	private Long conta_id;
	private String nome;
	private Float saldo;
	private Float qtdacoes;
	private int qtdnegociacoes;
	
	public Long getConta_id() {
		return conta_id;
	}
	public void setConta_id(Long conta_id) {
		this.conta_id = conta_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Float getSaldo() {
		return saldo;
	}
	public void setSaldo(Float saldo) {
		this.saldo = saldo;
	}
	public Float getQtdacoes() {
		return qtdacoes;
	}
	public void setQtdacoes(Float qtdacoes) {
		this.qtdacoes = qtdacoes;
	}
	public int getQtdnegociacoes() {
		return qtdnegociacoes;
	}
	public void setQtdnegociacoes(int qtdnegociacoes) {
		this.qtdnegociacoes = qtdnegociacoes;
	}
	
	
	
}
